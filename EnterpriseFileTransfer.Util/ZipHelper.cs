﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using System.IO;

namespace EnterpriseFileTransfer.Util
{
    public static class ZipHelper
    {
        public readonly static string ZipPassword = ConfigurationManager.AppSettings["ZipPassword"].ToString();
        
        public static bool Zip(string filePath,string TargetDirectory)
        {
            if (File.Exists(filePath))
                throw new ApplicationException("");
            if (Directory.Exists(TargetDirectory))
                throw new ApplicationException("");

            bool rv = false;
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.Password = ZipPassword;
                    zip.Comment = "";
                    zip.AddFile(filePath, "Files");
                    zip.Save(TargetDirectory);
                }
            }
            catch (Exception ex)
            {
                rv = false;
            }

            return rv;
        }

        public static bool Zip(string[] fileNames,string TargetDirectory)
        {
            bool rv = false;
            try
            {
                for (int i = 0; i < fileNames.Count(); i++)
                {
                    Zip(fileNames[i], TargetDirectory);
                }
            }
            catch (Exception ex)
            {
                rv = false;
            }

            return rv;
        }

        public static bool UnZip(string TargetDirectory,string filePath) 
        {
            bool rv = false;
            try
            {
                using (ZipFile zip=ZipFile.Read(filePath))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.ExtractWithPassword(TargetDirectory, ZipPassword);
                    }
                }
            }
            catch (Exception ex)
            {
                rv = false;
            }

            return rv;
        }

    }
}

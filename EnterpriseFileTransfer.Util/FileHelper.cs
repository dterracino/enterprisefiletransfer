﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer.Util
{
    public static class FileHelper
    {
        public static string GetStringByFile(string path) 
        {
            if (String.IsNullOrEmpty(path))
                throw new ApplicationException("");

            FileStream stream = File.OpenRead(path);
            Byte[] data = new Byte[stream.Length];
            try
            {   
                stream.Read(data, 0, Convert.ToInt32(stream.Length));
            }
            finally 
            {
                stream.Close();    
            }
            return ObjectHelper.GetString(data);
        }

        public static void SetFileByString(string targetPath,string data) 
        {
            if (String.IsNullOrEmpty(targetPath))
                throw new ApplicationException("");

            byte[] newdata = ObjectHelper.GetBytes(data);
            
            if (!File.Exists(targetPath))
                File.WriteAllBytes(targetPath, newdata);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer.Util
{
    
    public class FileWatcher
    {
        // Wrapper event customization
        public event ErrorEventHandler OnError;
        public event FileSystemEventHandler OnCreated;

        // for opertaional jobs(methods)
        public Action CreatedAction = null;
        public Action ErrorAction = null;
        public Action RenamedAction = null;
        public Action DeletedAction = null;
        public Action ChangedAction = null;

        private FileSystemWatcher _watcher;

        private bool IsWatching;
        private StringBuilder strMsg = new StringBuilder();

        private bool IncludeSubFolder = false; // SubFolder Check 
        private bool DirectoryCheck; // Directory // Folder
        // !DirectoryCheck           // File        
        private string path;

        //Singleton pattern   
        private static FileWatcher _current = null;
        private static object _lockObj = new object();

        public static FileWatcher Current(string path, bool isDirectory, bool includeSubFolder)
        {
            if (_current == null)
            {
                lock (_lockObj)
                {
                    if (_current == null)
                    {
                        _current = new FileWatcher(path, isDirectory, includeSubFolder);
                    }
                }
            }

            return _current;
        }

        private FileWatcher(string path, bool isDirectory, bool includeSubFolder)
        {
            this.IsWatching = false;

            this.path = path;
            this.DirectoryCheck = isDirectory;
            this.IncludeSubFolder = includeSubFolder;
        }

        public void Watch()
        {
            if (IsWatching)
            {
                IsWatching = false;
                _watcher.EnableRaisingEvents = false;
                _watcher.Dispose();
                //strMsg.Append("Stop Watching....\n");
            }
            else
            {
                IsWatching = true;
                //strMsg.Append("Start Watching ...\n");

                _watcher = new FileSystemWatcher();
                if (DirectoryCheck)
                {
                    //Directory
                    _watcher.Filter = "*.*";
                    _watcher.Path = path + "\\";

                    if (IncludeSubFolder)
                    {
                        _watcher.IncludeSubdirectories = true;
                    }
                }
                else
                {
                    //File
                    _watcher.Filter = path.Substring(path.LastIndexOf('\\') + 1);
                    _watcher.Path = path.Substring(0, path.Length - _watcher.Filter.Length);
                }

                _watcher.NotifyFilter = NotifyFilters.LastAccess |
                                        NotifyFilters.LastWrite |
                                        NotifyFilters.FileName |
                                        NotifyFilters.DirectoryName;

                _watcher.Changed += _watcher_Changed;
                _watcher.Created += _watcher_Created;
                _watcher.Deleted += _watcher_Deleted;
                _watcher.Renamed += _watcher_Renamed;
                _watcher.Error += _watcher_Error;

                _watcher.EnableRaisingEvents = true;
            }
        }

        void _watcher_Renamed(object sender, RenamedEventArgs e)
        {
            strMsg.Remove(0, strMsg.Length);
            strMsg.Append(e.OldFullPath);
            strMsg.Append(" ");
            strMsg.Append(e.ChangeType.ToString());
            strMsg.Append(" ");
            strMsg.Append("to ");
            strMsg.Append(e.Name);
            strMsg.Append("    ");
            strMsg.Append(DateTime.Now.ToString());

            //File
            if (!this.DirectoryCheck)
            {
                _watcher.Filter = e.Name;
                _watcher.Path = e.FullPath.Substring(0, e.FullPath.Length - _watcher.Filter.Length);
            }

            //Console.WriteLine(strMsg.ToString());
            if (RenamedAction != null)
                RenamedAction();
        }

        void _watcher_Error(object sender, ErrorEventArgs e)
        {
            // Logging or email sending
            if (OnError != null)
                OnError(this, e);            

            if (ErrorAction != null)
                ErrorAction();
        }

        void _watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            strMsg.Remove(0, strMsg.Length);
            strMsg.Append(e.FullPath);
            strMsg.Append(" ");
            strMsg.Append(e.ChangeType.ToString());
            strMsg.Append("    ");
            strMsg.Append(DateTime.Now.ToString());

            // Logging

            //Console.WriteLine(strMsg.ToString());
            if (DeletedAction != null)
                DeletedAction();
        }

        void _watcher_Created(object sender, FileSystemEventArgs e)
        {
            // Logging or email sending

            strMsg.Remove(0, strMsg.Length);
            strMsg.Append(e.FullPath);
            strMsg.Append(" ");
            strMsg.Append(e.ChangeType.ToString());
            strMsg.Append("    ");
            strMsg.Append(DateTime.Now.ToString());

            if (OnCreated != null)
                OnCreated(this, e);
            
            //Console.WriteLine(strMsg.ToString());
            if (CreatedAction != null)
                CreatedAction();

        }

        void _watcher_Changed(object sender, FileSystemEventArgs e)
        {
            // Logging or email sending

            strMsg.Remove(0, strMsg.Length);
            strMsg.Append(e.FullPath);
            strMsg.Append(" ");
            strMsg.Append(e.ChangeType.ToString());
            strMsg.Append("    ");
            strMsg.Append(DateTime.Now.ToString());

            //Console.WriteLine(strMsg.ToString());
            if (ChangedAction != null)
                ChangedAction();
        }
    }
}

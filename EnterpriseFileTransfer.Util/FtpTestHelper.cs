﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer.Util
{
    public static class FtpTestHelper
    {
        public static bool TestConnect(string ftpRequestUrl, string ftpUserName, string ftpPassword)
        {
            bool rv = false;
            if (!TestUri(ftpRequestUrl))
                return false;

            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpRequestUrl));
            request.Credentials = new NetworkCredential(ftpUserName, ftpRequestUrl);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                rv = true;
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    rv = true;
                }
                else
                {
                    rv = false;
                }
            }
            return rv;
        }

        public static bool TestUri(string ftpUrl)
        {
            // ftp://DomainName.com/
            // ftp://12.12.12.12/
            Regex regex = new Regex("^(ftp)://.+$");
            return regex.IsMatch(ftpUrl);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer.Util
{
    public static class EventLogger
    {
        private const string LogSourceName = "";
        private const string LogName = "";
        private static EventLog log = null;

        private static void CheckLogger()
        {
            if (log.Equals(null))
                log = new EventLog();

            if (EventLog.SourceExists(LogSourceName) == false)
            {
                EventLog.CreateEventSource(LogSourceName, LogName);
            }
        }
        
        public static void Write(string msg)
        {
            Write(msg, EventLogEntryType.Information);
        }

        public static void Write(string msg,EventLogEntryType type)
        {
            CheckLogger();
            log.Source = LogSourceName;
            log.WriteEntry(msg, type);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer.Util
{
    public static class SecureHelper
    {
        static public string Encode(string data)
        {
            byte[] dataByteArray = System.Text.ASCIIEncoding.ASCII.GetBytes(data);
            string encodedData = Convert.ToBase64String(dataByteArray);
            return encodedData;
        }

        static public string Decode(string encodedData)
        {
            byte[] encodedDataByteArray = System.Convert.FromBase64String(encodedData);
            string originalData = ASCIIEncoding.ASCII.GetString(encodedDataByteArray);
            return originalData;
        }

        static public bool Check(string data,string encodedData) 
        {
            return String.Equals(data, Decode(encodedData));
        }
    }
}

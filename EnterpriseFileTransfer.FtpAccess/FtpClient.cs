﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace EnterpriseFileTransfer.FtpAccess
{
    // Actual Class - FtpClient
    public class FtpClient
        : IDisposable, IFtpOperation
    {
        private FtpWebRequest _ftpRequest;
        public FtpCredential _ftpCredential;

        public FtpClient()
        {

        }

        public FtpClient(FtpCredential ftpCredential)
        {
            this._ftpCredential = ftpCredential;
            this.Connect(); // To connect automatically 
        }

        public FtpClient(string ftpRequestUrl, string ftpUserName, string ftpPassword)
        {
            this._ftpCredential.RequestUrl = ftpRequestUrl;
            this._ftpCredential.UserName = ftpUserName;
            this._ftpCredential.Password = ftpPassword;
            this.Connect(); // To connect automatically  
        }

        public void Connect()
        {
            if (this._ftpCredential.Equals(null))
                throw new FtpException("");

            this._ftpRequest = (FtpWebRequest)FtpWebRequest.Create(this._ftpCredential.RequestUrl);
            this._ftpRequest.Credentials = new NetworkCredential(this._ftpCredential.UserName, this._ftpCredential.Password);
            this._ftpRequest.KeepAlive = false;
            this._ftpRequest.UseBinary = true;
            this._ftpRequest.UsePassive = true;
        }

        public void Download(string path)
        {
            _ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            try
            {
                FtpWebResponse response = (FtpWebResponse)_ftpRequest.GetResponse();
                using (Stream sr = (Stream)response.GetResponseStream())
                {
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024]; //downloads in chuncks

                    while (true)
                    {
                        //Try to read the data
                        int bytesRead = sr.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            break;
                        }
                        else
                        {
                            //Write the downloaded data
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                    {
                        fs.Write(memStream.ToArray(), 0, (int)memStream.Length);
                    }
                }
                response.Close();
            }
            catch (FtpException ex)
            {

            }
        }

        public void Upload(string path)
        {
            _ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
            try
            {
                using (FileStream sr = File.OpenRead(path))
                using (Stream st = _ftpRequest.GetRequestStream())
                {
                    MemoryStream memStream = new MemoryStream();
                    memStream.SetLength(sr.Length);
                    sr.Read(memStream.GetBuffer(), 0, (int)memStream.Length);
                    st.Write(memStream.ToArray(), 0, (int)memStream.Length);
                    st.Flush();
                }
                FtpWebResponse response = (FtpWebResponse)_ftpRequest.GetResponse();
                response.Close();
            }
            catch (FtpException ex)
            {

            }
        }

        public void Dispose() { }

        public string[] GetFiles()
        {
            List<string> files = new List<string>();
            _ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

            string[] items;

            using (FtpWebResponse response = (FtpWebResponse)_ftpRequest.GetResponse())
            {
                TextReader st = new StreamReader(response.GetResponseStream());
                string responseText = st.ReadToEnd();
                response.Close();
                items = responseText.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }

            foreach (string item in items)
            {
                files.Add(item);
            }
            return (string[])files.ToArray();
        }
    }
}

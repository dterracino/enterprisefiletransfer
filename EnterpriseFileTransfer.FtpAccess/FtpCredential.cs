﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnterpriseFileTransfer.FtpAccess
{
    public class FtpCredential
    {
        public string RequestUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        
        public FtpCredential(string RequestUrl, string UserName, string Password)
        {
            this.RequestUrl = RequestUrl;
            this.UserName = UserName;
            this.Password = Password;
        }
    }
}

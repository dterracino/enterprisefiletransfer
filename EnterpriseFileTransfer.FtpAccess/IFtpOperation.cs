﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnterpriseFileTransfer.FtpAccess
{
    internal interface IFtpOperation
    {
        void Download(string path);
        void Upload(string path);
        string[] GetFiles();
    }
}

﻿using EnterpriseFileTransfer.Util;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections;

namespace EnterpriseFileTransfer.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //// test 1 customfilewatcher
            //FileWatcher watcher = FileWatcher.Current(@"C:\Test_watcher", true, false);
            //watcher.Watch();

            //// test 2
            //FileWatcher watcher2 = FileWatcher.Current(@"C:\Test_watcher", true, false);
            //watcher2.CreatedAction = TryIt;
            //watcher2.Watch();

            // test 3 - System dll
            //string startPath = @"C:\DAILY_LOGS";
            //string zipPath = @"C:\Zipped_DAILY_LOGS.zip";
            //string extractPath = @"C:\DAILY_LOGS\EXTRACT";
            //ZipFile.CreateFromDirectory(startPath, zipPath);
            //ZipFile.ExtractToDirectory(zipPath, extractPath);

            // test 4 - ThirdParty Dll - Ionic.Zip
            //string filepath = @"C:\DAILY_LOGS\test.xlsx";
            //using (ZipFile zip = new ZipFile())
            //{
            //    zip.Password = "deneme";
            //    zip.Comment = "";
            //    zip.AddFile(filepath, "Files");
            //    zip.Save("C:\\MyZipFile.zip");
            //}

            //string filepath = "C:\\MyZipFile.zip";
            //string TargetDirectory = @"C:\My\";

            //using (ZipFile zip = ZipFile.Read(filepath)) 
            //{
            //    foreach (ZipEntry e in zip)
            //    {
            //        e.ExtractWithPassword(TargetDirectory, "deneme");
            //    }
            //}

            //Timer timer = new Timer();
            //timer.Enabled = true;
            //timer.Interval = 5000;
            //timer.Elapsed += timer_Elapsed;
            //timer.Start();

            //string ftpUrl = "ftp://12.12.12.12/";
            //FtpTestHelper.TestUri(ftpUrl);

            string e = @"C:\denemeci.doc";
            string y = @"C:\test\denemeci.doc";
            string BinaryData = String.Empty;

            FileStream stream = File.OpenRead(e);
            Byte[] data = new Byte[stream.Length];
            stream.Read(data, 0, Convert.ToInt32(stream.Length));
            stream.Close();

            // 1
            Byte[] data2 = File.ReadAllBytes(e);

            //2
            BinaryData = GetString(data2);
            
            //3
            byte[] newData = GetBytes(BinaryData);

            //4
            File.WriteAllBytes(y, newData);


            //Console.WriteLine(BinaryData);
            Console.ReadLine();
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("İt's working... :D - {0} "+ e.SignalTime);

        }

        public static void TryIt() 
        {
            Console.WriteLine("İt's working... :D ");
        }
    }
}

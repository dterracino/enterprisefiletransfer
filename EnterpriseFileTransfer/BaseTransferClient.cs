﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseFileTransfer.FtpAccess;

namespace EnterpriseFileTransfer
{
    public abstract class BaseTransferClient
         : ITransferClient
    {
        protected FileProcessor fileProcessor;
        protected FtpClient ftpClient;
        public BaseTransferClient()
        {
            ftpClient = new FtpClient();
            fileProcessor = new FileProcessor();
            fileProcessor.StartProcess(FileTransferConstants.FollowedDirectory, FileTransferConstants.TargetDirectory);
        }

        public abstract bool Connect(FtpCredential credential);
        public abstract bool Connect();
        public virtual void DoTransfer() { }

    }
}

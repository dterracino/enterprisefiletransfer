﻿using EnterpriseFileTransfer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseFileTransfer.FtpAccess;
using System.IO;

namespace EnterpriseFileTransfer
{
    public class FileReceiver
        : BaseTransferClient
    {
        // ftpp server sender credential informations
        public override bool Connect(FtpCredential credential)
        {
            ftpClient._ftpCredential = credential;            
            
            return true;
        }

        public override bool Connect()
        {
            ftpClient._ftpCredential = FileTransferConstants.GetSenderCredential;

            return true;
        }

        public override void DoTransfer()
        {
            // Ftp GetFiles
            List<string> receiverFiles = ftpClient.GetFiles().ToList();

            string followedDirectory = FileTransferConstants.FollowedDirectory;

            if(receiverFiles.Contains(followedDirectory))
            {
                FileWatcher watcher = FileWatcher.Current(followedDirectory, true, false);
                watcher.OnCreated += watcher_OnCreated;
                watcher.OnError += watcher_OnError;
                watcher.Watch();
            }
            else
            {
                //Or create spesific name folder in the receiver's ftp server
            }

        }

        void watcher_OnError(object sender, ErrorEventArgs e)
        {
            // on the Error case operations
        }

        void watcher_OnCreated(object sender, FileSystemEventArgs e)
        {
            string fullPath = e.FullPath;
            string filaname = e.Name;

            try
            {
                ftpClient.Download(fullPath);
            }
            catch (Exception)
            {                
                throw;
            }
        }
        
    }
}

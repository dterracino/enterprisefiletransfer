﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseFileTransfer.FtpAccess;

namespace EnterpriseFileTransfer
{
    public class FileSender
       : BaseTransferClient
    {
        // ftpp server receiver credential informations
        public override bool Connect(FtpCredential credential)
        {
            ftpClient._ftpCredential = credential;

            return true;
        }

        public override bool Connect()
        {
            ftpClient._ftpCredential = FileTransferConstants.GetReceiverCredential;

            return true;
        }
    }
}

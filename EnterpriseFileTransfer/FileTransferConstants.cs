﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseFileTransfer.FtpAccess;

namespace EnterpriseFileTransfer
{
    public static class FileTransferConstants
    {
        private static string SenderFtpServerUrl = ConfigurationManager.AppSettings[""].ToString();
        private static string SenderFtpServerUserName = ConfigurationManager.AppSettings[""].ToString();
        private static string SenderFtpServerPassword = ConfigurationManager.AppSettings[""].ToString();

        private static string ReceiverFtpServerUrl = ConfigurationManager.AppSettings[""].ToString();
        private static string ReceiverFtpServerUserName = ConfigurationManager.AppSettings[""].ToString();
        private static string ReceiverFtpServerPassword = ConfigurationManager.AppSettings[""].ToString();

        public static string FollowedDirectory = ConfigurationManager.AppSettings[""].ToString();
        public static string TargetDirectory = ConfigurationManager.AppSettings[""].ToString();

        public static int TransferTimeInterval = 10000;

        public static FtpCredential GetSenderCredential
        {
            get { return new FtpCredential(SenderFtpServerUrl, SenderFtpServerUserName, SenderFtpServerPassword); }
        }

        public static FtpCredential GetReceiverCredential
        {
            get { return new FtpCredential(ReceiverFtpServerUrl, ReceiverFtpServerUserName, ReceiverFtpServerPassword); }
        }

    }
}

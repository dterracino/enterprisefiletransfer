﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    public class TransferParameters
    {
        public IDictionary<string, Object> _prms = null;

        public TransferParameters()
        {
            this._prms = new Dictionary<string, Object>();
        }


        public void Add(string key, object value)
        {
            this.Remove(key);

            if (!String.IsNullOrEmpty(key))
                this._prms.Add(key, value);
        }

        public void Remove(string key)
        {
            if (this._prms.ContainsKey(key) && !String.IsNullOrEmpty(key))
                this._prms.Remove(key);
        }

        public object TryGetValue(string key)
        {
            object obj;
            _prms.TryGetValue(key, out obj);
            return obj;
        }

        public void Clear()
        {
            this._prms.Clear();
        }
    }
}

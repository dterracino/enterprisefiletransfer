﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    public abstract class BaseTransfer
            : ITransfer
    {
        public delegate void ProcessorStartingEventHandler(params Object[] param);
        public delegate void ProcessorStartedEventHandler(params Object[] param);
        public delegate void ProcessorCompletedEventHandler(params Object[] param);
        public delegate void ProcessorErrorEventHandler(params Object[] param);

        public ProcessorStartingEventHandler ProcessorStarting = null;
        public ProcessorStartedEventHandler ProcessorStarted = null;
        public ProcessorCompletedEventHandler ProcessorCompleted = null;
        public ProcessorErrorEventHandler ProcessorError = null;

        // you can set status in the processor class 
        public TransferStatus.Current Status { get; private set; }

        // parameter variables
        public TransferParameters _parameters = null;
        public TransferParameters Parameters
        {
            set { _parameters = value; }
        }

        // threading mechanism (Semaphore)
        private static Semaphore _semaphore = null;
        private static object _semaphoreLockObj = new object();

        // singleton pattern for semaphore
        private void InitializeSemaphore(int jobCountLimit)
        {
            if (_semaphore == null)
            {
                lock (_semaphoreLockObj)
                {
                    if (_semaphore == null)
                    {
                        _semaphore = new Semaphore(jobCountLimit, jobCountLimit);
                    }
                }
            }
        }

        // ctor methods
        public BaseTransfer(TransferParameters param, int jobCountLimit)
        {
            this._parameters = param;
            InitializeSemaphore(jobCountLimit);
        }

        public BaseTransfer(TransferParameters param)
            : this(param, 3)
        { }

        // Real Job Method 
        protected abstract void Run(object param, out object[] output);
        
        private void Job(object param)
        {
            object[] output = null;

            // started loging
            this.Status = TransferStatus.Current.Started;
            if (ProcessorStarted != null)
                ProcessorStarted(this.GetType());

            try
            {
                _semaphore.WaitOne();
                Run(param, out output);
            }
            catch (Exception ex)
            {
                this.Status = TransferStatus.Current.Error;
                if (ProcessorError != null)
                {
                    ProcessorError(this.GetType(), ex);
                }
                // error logging
            }
            finally
            {
                _semaphore.Release();
            }

            // completing loging
            this.Status = TransferStatus.Current.Completing;

            if (ProcessorCompleted != null)
            {
                ProcessorCompleted(this.GetType(), output);
            }

            // completed loging
            this.Status = TransferStatus.Current.Completed;
        }

        public void ExecuteTranfer()
        {
            // starting loging for real job's log table
            this.Status = TransferStatus.Current.Starting;

            if (ProcessorStarting != null)
                ProcessorStarting(this.GetType());

            ThreadPool.QueueUserWorkItem(new WaitCallback(Job), this._parameters);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    // Customized Processor Class for custom jobs - for instance FileTransfer :D
    public class FileTransfer
        : BaseTransfer
    {
        public FileTransfer(TransferParameters param, int jobLimit)
            : base(param, jobLimit)
        { }

        public FileTransfer(TransferParameters param)
            : base(param)
        { }

        protected override void Run(object param, out object[] output)
        {
            FileTransferJob job = new FileTransferJob();
            output = job.Start(param as TransferParameters);
        }
    }
}

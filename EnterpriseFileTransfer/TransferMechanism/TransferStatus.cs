﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    public class TransferStatus
    {
        public enum Current
        {
            Starting = 1,
            Started = 2,
            Completing = 3,
            Completed = 4,
            Error = 5
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace EnterpriseFileTransfer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("File scanning is starting...");

            // all procedures is starting 
            FileTransfer transfer = new FileTransfer(new TransferParameters());
            transfer.ExecuteTranfer();
            
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace EnterpriseFileTransfer
{
    public class FileTransferJob
         : BaseJob
    {
        public override object[] Start(TransferParameters paramList = null)
        {
            try
            {
                // fact job operations

                Timer fileTransferTimer = new Timer();
                fileTransferTimer.Elapsed += fileTransferTimer_Elapsed;
                fileTransferTimer.Enabled = true;
                fileTransferTimer.Interval = FileTransferConstants.TransferTimeInterval;
                fileTransferTimer.Start();

                Console.WriteLine("Real Operational Job...");
            }
            catch (Exception ex)
            {
                return new object[] { ex.Message };
            }
            return null;
        }

        static void fileTransferTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            FileReceiver receiver = new FileReceiver();
            receiver.Connect(FileTransferConstants.GetReceiverCredential);
            receiver.DoTransfer();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    public interface IJob
    {
        object[] Start(TransferParameters paramList = null);
    }
}

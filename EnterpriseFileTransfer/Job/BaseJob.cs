﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseFileTransfer
{
    public abstract class BaseJob
       : IJob
    {
        public abstract object[] Start(TransferParameters paramList = null);
    }
}

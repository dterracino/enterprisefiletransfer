﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseFileTransfer.Util;

namespace EnterpriseFileTransfer
{
    public interface IFileProcessor
    {
        void StartProcess(string followedDirectory,string targetDirectory);
    }

    public class FileProcessor
        :IFileProcessor
    {

        public void StartProcess(string followedDirectory, string targetDirectory)
        {
            FileWatcher watcher = FileWatcher.Current(followedDirectory, true, false);
            watcher.OnCreated += watcher_OnCreated;
            watcher.Watch();
        }

        void watcher_OnCreated(object sender, FileSystemEventArgs e)
        {
            if (File.Exists(e.FullPath))
            {
                //string pureString = FileHelper.GetStringByFile(e.FullPath);
                //string encoded = SecureHelper.Encode(pureString);
                ZipHelper.Zip(e.FullPath, "");
            }
        }
    }
}
